function problem3(inventory){
    if(!inventory || inventory.length==0){
        return [];
    }
    updatedinventory=inventory;
    //here we have to make all the car model to the lower case because we have to sort//
    updatedinventory.sort(function(a,b){
        if(a.car_model.toLowerCase() < b.car_model.toLowerCase()){
            return -1;
        }
        else if(a.car_model.toLowerCase()>b.car_model.toLowerCase()){
            return 1;
        }else{
            return 0;
        }
    });
        return updatedinventory;
}

module.exports=problem3;
